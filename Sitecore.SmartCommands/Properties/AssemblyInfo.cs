﻿#region Usings

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#endregion

[assembly: AssemblyTitle("Sitecore.SmartCommands")]
[assembly: AssemblyCompany("Sitecore Corporation")]
[assembly: AssemblyCopyright("Copyright © 2001-2015 Sitecore Corporation")]
[assembly: AssemblyTrademark("Sitecore® is a registered trademark of Sitecore Corporation")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("7.0.0.0")]
[assembly: AssemblyFileVersion("7.0.0.0")]
[assembly: AssemblyInformationalVersion("7.0 rev. YYMMDD")]

[assembly: InternalsVisibleTo("Sitecore.SmartCommands.UnitTests")]
